package io.muzoo.ssc.zork.item;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Items {
    public List<Item> allItemList;

    public Items(){
        allItemList = new ArrayList<>();

        Scanner itemsScanner = null;
        try {
            itemsScanner = new Scanner(new File("src/Items.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (itemsScanner.hasNext()) {
            String Items = itemsScanner.nextLine();
            String itemKind = Items.split(",")[0].trim();
            String itemName = Items.split(",")[1].trim();
            int itemAtk = Integer.parseInt(Items.split(",")[2].trim());
            if(itemKind.equals("Weapon")){
                Weapon weapon = new Weapon(itemName,itemAtk,10);
                allItemList.add(weapon);
            }else{
                Potion potion = new Potion(itemName, itemAtk, 1);
                allItemList.add(potion);
            }
        }
        itemsScanner.close();
    }
}
