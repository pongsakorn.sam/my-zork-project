package io.muzoo.ssc.zork.item;

import io.muzoo.ssc.zork.player.Player;

public abstract class Item {

    public String itemName;
    public int durability;
    public int ATK;
    public boolean usable = true;

    public Item(String itemName,int ATK,int durability){
        this.itemName = itemName;
        this.ATK = ATK;
        this.durability = durability;
    }

    public Item(Item item){
        this.itemName = item.itemName;
        this.ATK = item.ATK;
        this.durability = item.durability;
    }

    public int getATK(){
        return this.ATK;
    }
    public int getDurability() { return this.durability; }
    public String getItemName(){
        return this.itemName;
    }

    public void reduceDurability(){
        this.durability--;
    }
    public boolean isUsable(){
        if (this.durability <= 0){
            this.usable = false;
        }
        return this.usable;
    }
    public abstract void use(Player player);
    public abstract void printItemStat();
}
