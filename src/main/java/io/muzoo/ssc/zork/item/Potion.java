package io.muzoo.ssc.zork.item;
import io.muzoo.ssc.zork.player.Player;

public class Potion extends Item{



    public Potion(String itemName, int ATK, int durability) {
        super(itemName, ATK, durability);
    }

    @Override
    public void printItemStat(){
        System.out.println(itemName +" --- [HP OFFER: " + ATK +", " + "Durability: " + durability+"]");
    }

    @Override
    public void use(Player player) {
        this.usable = false;
        player.HP += ATK;
        System.out.println("Drinking the potion, player current HP: " + player.HP);
    }
}
