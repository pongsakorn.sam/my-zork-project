package io.muzoo.ssc.zork.item;

import io.muzoo.ssc.zork.player.Player;

public class Weapon extends Item{

    public Weapon(String itemName, int ATK, int durability) {
        super(itemName, ATK, durability);
    }

    @Override
    public void printItemStat() {
        System.out.println(itemName +" --- [ATK: " + ATK +", " + "Durability: " + durability+"]");
    }

    @Override
    public void use(Player player) {
        player.ATK += this.getATK();
        this.reduceDurability();
        System.out.println("Using " + this.itemName + ", player's ATK is increased by: " + this.getATK());
    }
}
