package io.muzoo.ssc.zork.factory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandlineParser {


    private List<String> allCommandSortedByLength = new ArrayList<>();

    {
        allCommandSortedByLength.addAll(CommandFactory.getAllCommand());
        allCommandSortedByLength.sort((o1, o2) -> o2.length() - o1.length());
    }


    public String matchInputToCommand(String input) {
        for (String command : allCommandSortedByLength) {
            if (input.toLowerCase().startsWith(command.toLowerCase())) {
                return command;

            }
        }
        return null;
    }


    public List<String> parse(String input) {
        String cleanedInput = input.trim();
        String cmd = matchInputToCommand(cleanedInput);
        Command command = CommandFactory.get(cmd);
        if (command != null) {
            if (command.numArgs() > 0) {
                String[] strings = input.split(" ", command.numArgs()+1);
                List<String> x = Arrays.asList(strings);
                List<String> result = new ArrayList<>();
                result.add(command.getCommand());
                int count = input.length() - result.get(0).length();
                if(count > 0){
                    if(!result.contains(x.get(x.size()-1))) result.add(x.get(x.size()-1));
                }else{
                    result.add(" ");
                }
                return result;
            }else{
                return Arrays.asList(cmd);
            }
        }
        return null;
    }

    public static void main(String[] args) {
        CommandlineParser cmp = new CommandlineParser();
        List<String> x = cmp.parse("ok");
        System.out.println(x);
    }
}