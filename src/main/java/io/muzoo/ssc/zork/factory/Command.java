package io.muzoo.ssc.zork.factory;


import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public interface Command {
    int numArgs();
    String getCommand();
    String getDescription();
    boolean canUseInGame();
    void execute(Zork game, List<String> input);
}
