package io.muzoo.ssc.zork.factory;
import io.muzoo.ssc.zork.command.*;
import io.muzoo.ssc.zork.game.Zork;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

public class CommandFactory {

    private static final List<Class<? extends Command>> REGISTERED_COMMANDS = Arrays.asList(
            AttackWithCommand.class,    // player's attack with
            AvailableExitCommand.class, // available exit for the player's current room
            DrinkCommand.class,         // player's drink potion
            DropCommand.class,          // player's drop item
            ExitCommand.class,          // exit the game
            GoCommand.class,            // player's go/move method
            HelpCommand.class,          // help, print all command available at in current game state
            InfoCommand.class,          // print all stat of players and current room
            LoadCommand.class,          // load from save
            MapCommand.class,           // print playing map info
            MapsCommand.class,          // print all available map to choose to play from
            PlayCommand.class,          // start the game with {map name}
            QuitCommand.class,          // quit the game and return to main menu
            SaveCommand.class,          // save the game to file {name.txt}
            TakeCommand.class           // player's take item
    );

    private static final Map<String, Command> COMMAND_MAP = new HashMap<>();

    static {{
        for(Class<? extends Command> commandClass: REGISTERED_COMMANDS){
            try{
                Command command = commandClass.getDeclaredConstructor().newInstance();
                COMMAND_MAP.put(command.getCommand(), command);
            }catch (InstantiationException e){
                e.printStackTrace();
            }catch (IllegalAccessException e){
                e.printStackTrace();
            }catch(InvocationTargetException e){
                e.printStackTrace();
            }catch(NoSuchMethodException e){
                e.printStackTrace();
            }
        }
    }}

    public static Command get(String command){
        return COMMAND_MAP.get(command);
    }

    public static List<String> getAllCommand(){
        return COMMAND_MAP.keySet().stream().collect(Collectors.toList());
    }


    public static void printOutCommand(Zork game){
        game.gameOutput.println("Available Commands: *******************************");
        for(Command command: COMMAND_MAP.values()){
            if(command.getCommand().equals("help")){
                game.gameOutput.println(command.getCommand() + " - " +command.getDescription());
                continue;
            }
            if(command.canUseInGame() == game.isInGame){
                game.gameOutput.println(command.getCommand() + " - " +command.getDescription());
            }
        }
        game.gameOutput.println("***************************************************");
    }

}
