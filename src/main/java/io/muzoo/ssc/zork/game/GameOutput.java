package io.muzoo.ssc.zork.game;

import java.util.List;

public class GameOutput {

    public void println(List<String> message) { System.out.println(message); }
    public void println(String message){
        System.out.println(message);
    }
    public void print(String message) {
        System.out.print(message);
    }
}
