package io.muzoo.ssc.zork.game;


import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.factory.CommandFactory;
import io.muzoo.ssc.zork.factory.CommandlineParser;
import io.muzoo.ssc.zork.item.Item;
import io.muzoo.ssc.zork.item.Items;
import io.muzoo.ssc.zork.item.Potion;
import io.muzoo.ssc.zork.item.Weapon;
import io.muzoo.ssc.zork.player.Monster;
import io.muzoo.ssc.zork.player.Player;
import io.muzoo.ssc.zork.map.Maps;
import io.muzoo.ssc.zork.room.Room;

import java.io.*;
import java.util.*;


public class Zork {

    private Items availableItem;
    private String mapName;
    private Room currentRoom;
    private Player player;
    private Map<String,Room> map;
    private Map<String, HashMap<String, String>> exits;
    private String fileMapLocation;
    private CommandlineParser commandParser;
    private Random Randomizer;
    private int mapMonsterCount;
    private CommandFactory commandFactory;
    public GameOutput gameOutput;
    public boolean isInGame;
    public Maps availableMap;

    public Zork(){
        availableMap = new Maps();
        map = new HashMap<>();
        exits = new HashMap<>();
        player = new Player();
        isInGame = false;
        commandParser = new CommandlineParser();
        availableItem = new Items();
        Randomizer = new Random();
        mapMonsterCount = 0;
        gameOutput = new GameOutput();
        commandFactory = new CommandFactory();
    }

    // ZORK SETTING STUFF
    // (INITIALIZING)
    private void initializeMap(String fileMapLocation, Boolean loadFromSave)  {
        this.mapMonsterCount = 0;
        Scanner roomScanner = null;
        try {
            roomScanner = new Scanner(new File(fileMapLocation));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (roomScanner.hasNext()){
            Room room = new Room();
            String roomName = roomScanner.nextLine();
            roomName = roomName.split(":")[1].trim();
            room.setRoomName(roomName);
            String roomDescription = roomScanner.nextLine();
            room.setDescription(roomDescription.split(":")[1].trim());
            String stringExitRooms = roomScanner.nextLine();
            String[] listExitRooms = stringExitRooms.split(":")[1].trim().split(",");
            // getting all exit with direction current room
            HashMap<String, String> directionMap = new HashMap<String,String>();
            for(String s: listExitRooms){
                String exitDirection = s.split("-")[0].trim().toUpperCase();
                String exitName = s.split("-")[1].trim();
                directionMap.put(exitDirection,exitName);
            }
            exits.put(roomName,directionMap);
            map.put(roomName,room);
            int monsterCount = Integer.parseInt(roomScanner.nextLine().split(":")[1].trim());
            int itemCount = Integer.parseInt(roomScanner.nextLine().split(":")[1].trim());
            if(loadFromSave != true){
                mapMonsterCount += monsterCount;
                spawnItems(room,itemCount);
                spawnMonsters(room,monsterCount);
            }
        }
        // after creating map, we create/put all the exits
        for (Room room: map.values()){
            HashMap<String, String> tempExits = exits.get(room.roomName);
            for(String direction : tempExits.keySet()){
                String roomName2 = tempExits.get(direction);
                Room exitRoom = map.get(roomName2);
                room.setExit(direction,exitRoom);
            }
        }
    }
    // (SPAWN STUFF)
    private void spawnMonsters(Room room,int count){
        for(int i = 0; i < count; i++){
            room.putMonster(new Monster("Zombies",2,100));
        }
    }
    private void spawnItems(Room room, int count){
        for(int i = 0; i < count ; i++){
            //random item from the item list
            Item item = availableItem.allItemList.get(Randomizer.nextInt(availableItem.allItemList.size()));
            //then we add it into the room
            if (item instanceof  Weapon){
                room.putItem(new Weapon(item.itemName,item.ATK,item.durability));
            }else{
                room.putItem(new Potion(item.itemName,item.ATK,item.durability));
            }
        }
    }

    // (GAME STATE)
    private void checkGameState(){
        if(player.isDead()){
            reset();
            gameOutput.println("Haha you lose, bye bye loser!");
            gameOutput.println("***************************************************");
            printWelcome();
        }
        if(mapMonsterCount == 0){
            reset();
            gameOutput.println("Congratulations you have beat my game!");
            gameOutput.println("***************************************************");
            printWelcome();
        }
    }
    private void reset(){
        map = new HashMap<>();
        exits = new HashMap<>();
        player = new Player();
        Randomizer = new Random();
        isInGame = false;
    } // reset will make everything new

    // (PRINT)
    private void printCurrentRoomInformation(){
        this.currentRoom.printRoomInfo();
    }
    private void printPlayerInformation(){
        gameOutput.println("Player is currently in: " + this.currentRoom.roomName);
        this.player.printPlayerStat();
    }
    private void printWelcome(){
        gameOutput.println("Welcome to the game");
        gameOutput.println("Please choose a map or load from save");
        gameOutput.println("Currently available map: " + availableMap.getMap().keySet());
    }


    // GAME LOOP
    public void run() {
        printWelcome();
        Scanner in = new Scanner(System.in);
        while (true) {
            gameOutput.print(">>> ");
            String s = in.nextLine();
            if(s.equals("")){
                continue;
            }
            s = s.toLowerCase();
            if (commandParser.parse(s) != null) {
                List<String> words = commandParser.parse(s);
                Command command = commandFactory.get(words.get(0));
                if (command.getCommand().equals("help")) {
                    command.execute(this, words.subList(1, words.size()));
                    continue;
                }
                if (command.canUseInGame() == isInGame) {
                    command.execute(this,words.subList(1, words.size()));
                } else {
                    gameOutput.println("This command cannot be use right now (check the game state, try to start/close the game first!)");
                }
            } else {
                gameOutput.println("Invalid Command!");
            }
            checkGameState();
        }
    }

    // COMMAND STUFF (PUBLIC)
    //(PLAYER COMMAND)
    public void attackWith(String itemName){
        Item currentItem = this.player.getItem(itemName);
        if(currentItem != null){
            List<Monster> monsterToRemove= new ArrayList<>();
            for(Monster monster:currentRoom.getRoomMonster()){
                player.attackWith(currentItem,monster);
                monster.attack(player);
                if(monster.isDead()){
                    monsterToRemove.add(monster);
                    player.increaseHP();
                    player.increaseATK();
                    mapMonsterCount--;
                }
            }
            for(Monster monster: monsterToRemove){
                currentRoom.getRoomMonster().remove(monster);
            }
            if(!currentItem.isUsable()){
                player.drop(currentItem);
                gameOutput.println(String.format("*** The item (%s) have been broken and will be remove from your inventory ***", currentItem.itemName));
            }
        }
    }
    public void go(String direction){
        direction = direction.toUpperCase();
        if(this.currentRoom.getExits().containsKey(direction)){
            Room nextRoom = this.currentRoom.getExits().get(direction);
            this.currentRoom = nextRoom;
            this.player.increaseHP();
            gameOutput.println("Arrived at: " + this.currentRoom.roomName);
            gameOutput.println("You found: " + this.currentRoom.getStringItems());
            gameOutput.println("You see: " + this.currentRoom.getRoomMonster().size() + " Monsters");
        }else{
            gameOutput.println("Please choose another direction, we cant go " + direction);
        }
    }
    public void drink(String itemName){
        Item currentItem = this.player.getItem(itemName);
        if(currentItem != null){
            if(currentItem instanceof Potion){
                currentItem.use(player);
            }else{
                gameOutput.println("You can only drink potion");
            }
        }
        if(!currentItem.isUsable()){
            player.drop(currentItem);
        }
    }
    public void drop(String itemName){
        Item currentItem = player.getItem(itemName);
        if(currentItem != null){
            player.drop(currentItem);
            currentRoom.putItem(currentItem);
            gameOutput.println("player dropped: " + currentItem.itemName);
        }
    }
    public void availableExit(){
        gameOutput.println("Player is currently in the room: " + this.currentRoom.roomName);
        gameOutput.println(this.currentRoom.exitString());
    }
    public void info(){
        printPlayerInformation();
        printCurrentRoomInformation();
        gameOutput.println("Map monster count: " + mapMonsterCount);
    }
    //(GAME COMMAND)
    public void printMapInformation(){
        for(Room room : map.values()){
            room.printRoomInfo();
        }
    }
    public void printAvailableMap(){
        gameOutput.println(Arrays.toString(availableMap.getMap().keySet().toArray()));
    }
    public void play(String mapName){
        if(availableMap.getMap().containsKey(mapName)){
            fileMapLocation = availableMap.getMap().get(mapName);
            this.mapName = mapName;
            initializeMap(fileMapLocation,false); // We did not load from save
            // Randomizing the starting room! ( we can set it also but I don't want)
            List<String> rooms = new ArrayList<String>(map.keySet());
            this.currentRoom = map.get(rooms.get(Randomizer.nextInt(map.size())));
            gameOutput.println("Player starting room is: " + this.currentRoom.roomName);
            isInGame = true;
        }else{
            gameOutput.println("we don't have this map yet please choose from what we have:" + availableMap.getMap().keySet());
        }
    }
    public void take(String itemName){
        // using isItemExist to return item if the room contain that particular item.
        Item item = this.currentRoom.getItem(itemName);
        if (item != null) {
            this.player.take(item);
            this.currentRoom.removeRoomItem(item);
            gameOutput.println("player obtained: " + item.itemName);
        }
    }
    public void quit(){
        try {
            save("AutoSave.txt");
            reset();
            gameOutput.println("You are back to the main menu!");
            gameOutput.println("To resume the game from last checkpoint please type: load AutoSave.txt");
            gameOutput.println("***************************************************");
            printWelcome();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void exit(){
        gameOutput.println("Bye bye, exiting the game");
        System.exit(1);
    }
    public void load(String filename) throws FileNotFoundException {
        // load from my template of save
        ArrayList<Item> playerItems = new ArrayList<>();
        Scanner saveScanner = new Scanner(new File(filename));
        String name;
        String roomName;
        int ATK;
        int HP;
        int durability;
        while (saveScanner.hasNext()){
            String s = saveScanner.nextLine();
            String[] loadObject = s.split(",");
            switch(loadObject[0]){
                case "map name":
                    mapName = loadObject[1].trim();
                    fileMapLocation = availableMap.getMap().get(loadObject[1].trim());
                    initializeMap(fileMapLocation,true);
                    break;
                case "player stat":
                    ATK = Integer.parseInt(loadObject[1]);
                    HP = Integer.parseInt(loadObject[2]);
                    this.player = new Player(ATK,HP);
                    break;
                case "player weapon":
                    name = loadObject[1];
                    ATK = Integer.parseInt(loadObject[2]);
                    durability = Integer.parseInt(loadObject[3]);
                    playerItems.add(new Weapon(name,ATK,durability));
                    break;
                case "player potion":
                    name = loadObject[1];
                    ATK = Integer.parseInt(loadObject[2]);
                    durability = Integer.parseInt(loadObject[3]);
                    playerItems.add(new Potion(name,ATK,durability));
                    break;
                case "room monster":
                    roomName = loadObject[1];
                    name = loadObject[2];
                    ATK = Integer.parseInt(loadObject[3]);
                    HP = Integer.parseInt(loadObject[4]);
                    map.get(roomName).putMonster(new Monster(name,ATK,HP));
                    break;
                case "room potion":
                    roomName = loadObject[1];
                    name = loadObject[2];
                    ATK = Integer.parseInt(loadObject[3]);
                    durability = Integer.parseInt(loadObject[4]);
                    map.get(roomName).putItem(new Potion(name,ATK,durability));
                    break;
                case "room weapon":
                    roomName = loadObject[1];
                    name = loadObject[2];
                    ATK = Integer.parseInt(loadObject[3]);
                    durability = Integer.parseInt(loadObject[4]);
                    map.get(roomName).putItem(new Weapon(name,ATK,durability));
                    break;
                case "player current room":
                    roomName = loadObject[1];
                    currentRoom = map.get(roomName);
                    break;
                case "map monster count":
                    mapMonsterCount = Integer.parseInt(loadObject[1]);
                    break;
            }
        }
        player.setInventory(playerItems);
        isInGame = true;
        gameOutput.println("Player starting room is: " + this.currentRoom.roomName);
    }
    public void save(String filename) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
        bw.write("map name," + mapName + "\n");
        bw.write("player current room," + currentRoom.roomName + "\n");
        bw.write("player stat," + player.ATK + "," + player.HP + "\n");
        for(Item item:player.getInventory()){
            String itemName = item.getItemName();
            int ATK = item.getATK();
            int durability = item.getDurability();
            if(item instanceof Weapon){
                bw.write("player weapon," + itemName + "," + ATK + "," + durability +"\n");
            }
            else
            {
                bw.write("player potion," + itemName + "," + ATK + "," + durability + "\n");
            }
        }
        for(Room room: map.values()){
            String roomName = room.roomName;
            for(Monster monster: room.getRoomMonster()){
                String monsterName = monster.getName();
                int ATK = monster.getATK();
                int HP = monster.getHP();
                bw.write("room monster," + roomName + "," + monsterName + "," + ATK + "," + HP+ "\n");
            }
            for(Item item: room.getRoomInventory()){
                String itemName = item.getItemName();
                int ATK = item.getATK();
                int durability = item.getDurability();
                if(item instanceof Weapon){
                    bw.write("room weapon," + roomName + "," + itemName + "," + ATK + "," + durability +"\n");
                }
                else
                {
                    bw.write("room potion," + roomName + "," + itemName + "," + ATK + "," + durability + "\n");
                }
            }
        }
        bw.write("map monster count," + mapMonsterCount + "\n");
        bw.close();
        System.out.println("Save successful, the game is saved as: " + filename);
    }


}

