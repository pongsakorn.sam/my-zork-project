package io.muzoo.ssc.zork.map;

import io.muzoo.ssc.zork.room.Room;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class Maps {
    private HashMap<String,String> availableMap;
    //availableMap will read from maps.txt (contain map name,map file location)
    //key = mapName
    //value = mapLocation
    public Maps() {
        availableMap = new HashMap<>();
        Scanner mapsScanner = null;
        try {
            mapsScanner = new Scanner(new File("src/maps.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (mapsScanner.hasNext()) {
            String mapDetail = mapsScanner.nextLine();
            String mapName = mapDetail.split(",")[0].trim();
            String mapFileLocation = mapDetail.split(",")[1].trim();
            availableMap.put(mapName,mapFileLocation);
        }
        mapsScanner.close();
    }

    public HashMap<String,String> getMap(){
        return this.availableMap;
    }

}
