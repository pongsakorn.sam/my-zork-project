package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class DropCommand implements Command {
    @Override
    public int numArgs() {
        return 1;
    }

    @Override
    public String getCommand() {
        return "drop";
    }

    @Override
    public String getDescription() {
        return "drop item of choice that the player currently carries";
    }

    @Override
    public boolean canUseInGame() {
        return true;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        if(input.get(0).equals(" ")){
            game.gameOutput.println("drop what?");
        }else{
            game.drop(input.get(0));
        }
    }
}
