package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;


public class ExitCommand  implements Command {

    @Override
    public int numArgs() {
        return 0;
    }
    @Override
    public boolean canUseInGame(){
        return false;
    }

    @Override
    public String getCommand() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit the game :(";
    }

    @Override
    public void execute(Zork game, List<String> input) {
        game.exit();
    }
}
