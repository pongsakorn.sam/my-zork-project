package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class MapCommand implements Command {
    @Override
    public int numArgs() {
        return 0;
    }

    @Override
    public String getCommand() {
        return "map";
    }

    @Override
    public String getDescription() {
        return "print the map's information";
    }

    @Override
    public boolean canUseInGame() {
        return true;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        game.printMapInformation();
    }
}
