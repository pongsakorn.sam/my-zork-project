package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;


public class InfoCommand implements Command {

    @Override
    public int numArgs() {
        return 0;
    }
    @Override
    public boolean canUseInGame(){
        return true;
    }

    @Override
    public String getCommand() {
        return "info";
    }

    @Override
    public String getDescription() {
        return "print out information of the player and the room that the player is currently in";
    }

    @Override
    public void execute(Zork game, List<String> input) {
        game.info();
    }
}
