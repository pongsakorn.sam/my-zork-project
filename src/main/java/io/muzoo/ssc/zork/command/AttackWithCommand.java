package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class AttackWithCommand implements Command {

    @Override
    public int numArgs() {
        return 2;
    }

    @Override
    public String getCommand() {
        return "attack with";
    }

    @Override
    public String getDescription(){
        return "(item) is used to attack all monster in the room";
    }

    @Override
    public boolean canUseInGame() {
        return true;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        if(input.get(0).equals(" ")){
            game.gameOutput.println("attack with what?");
        }else{
            game.attackWith(input.get(0));
        }
    }
}
