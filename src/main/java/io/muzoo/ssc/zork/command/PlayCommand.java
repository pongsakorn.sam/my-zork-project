package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class PlayCommand implements Command {

    @Override
    public int numArgs() {
        return 1;
    }

    @Override
    public String getCommand() {
        return "play";
    }

    @Override
    public String getDescription() {
        return "play new game, this command only available at when start the game";
    }

    @Override
    public boolean canUseInGame() {
        return false;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        if(input.get(0).equals(" ")){
            game.gameOutput.println("Please choose a map, available map: " + game.availableMap.getMap().keySet());
        }else{
            game.play(input.get(0));
        }
    }
}
