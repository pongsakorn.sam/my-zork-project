package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.factory.CommandFactory;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;


public class HelpCommand implements Command {

    @Override
    public int numArgs() {
        return 0;
    }
    @Override
    public boolean canUseInGame(){
        return false;
    }

    @Override
    public String getCommand() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "print all command available at current game state";
    }

    @Override
    public void execute(Zork game, List<String> input) {
        CommandFactory.printOutCommand(game);
    }
}
