package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.io.IOException;
import java.util.List;


public class SaveCommand implements Command {

    @Override
    public int numArgs() {
        return 1;
    }
    @Override
    public boolean canUseInGame(){
        return true;
    }

    @Override
    public String getCommand() {
        return "save";
    }

    @Override
    public String getDescription() {
        return "Save the game";
    }

    @Override
    public void execute(Zork game, List<String> input) {
        if(input.get(0).equals(" ")){
            game.gameOutput.println("please type in filename please :<");
        }else{
            try {
                game.save(input.get(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
