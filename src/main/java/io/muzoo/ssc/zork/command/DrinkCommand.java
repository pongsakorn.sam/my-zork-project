package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class DrinkCommand implements Command {

    @Override
    public int numArgs() {
        return 1;
    }

    @Override
    public String getCommand() {
        return "drink";
    }

    @Override
    public String getDescription() {
        return "drink a potion?";
    }

    @Override
    public boolean canUseInGame() {
        return true;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        if(input.get(0).equals(" ")){
            game.gameOutput.println("drink what?");
        }else{
            game.drink(input.get(0));
        }
    }
}
