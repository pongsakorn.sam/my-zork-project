package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class MapsCommand implements Command {
    @Override
    public int numArgs() {
        return 0;
    }

    @Override
    public String getCommand() {
        return "maps";
    }

    @Override
    public String getDescription() {
        return "Show the list of all the map that is currently available";
    }

    @Override
    public boolean canUseInGame() {
        return false;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        game.printAvailableMap();
    }
}
