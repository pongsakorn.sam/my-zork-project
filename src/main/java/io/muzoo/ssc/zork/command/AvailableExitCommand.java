package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;

public class AvailableExitCommand implements Command {

    @Override
    public int numArgs() {
        return 2;
    }

    @Override
    public String getCommand() {
        return "available exit";
    }

    @Override
    public String getDescription() {
        return "print out the available exit of the player current's room";
    }

    @Override
    public boolean canUseInGame() {
        return true;
    }

    @Override
    public void execute(Zork game, List<String> input) {
        game.availableExit();
    }
}
