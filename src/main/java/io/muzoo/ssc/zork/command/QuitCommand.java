package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.util.List;


public class QuitCommand implements Command {

    @Override
    public int numArgs() {
        return 0;
    }
    @Override
    public boolean canUseInGame(){
        return true;
    }

    @Override
    public String getCommand() {
        return "quit";
    }

    @Override
    public String getDescription() {
        return "quit the game and return to the menu";
    }

    @Override
    public void execute(Zork game, List<String> input) {
        game.quit();
    }
}
