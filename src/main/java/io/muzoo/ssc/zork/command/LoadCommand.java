package io.muzoo.ssc.zork.command;

import io.muzoo.ssc.zork.factory.Command;
import io.muzoo.ssc.zork.game.Zork;

import java.io.FileNotFoundException;
import java.util.List;


public class LoadCommand implements Command {

    @Override
    public int numArgs() {
        return 1;
    }
    @Override
    public boolean canUseInGame(){
        return false;
    }

    @Override
    public String getCommand() {
        return "load";
    }

    @Override
    public String getDescription() {
        return "Load a save from file";
    }

    @Override
    public void execute(Zork game, List<String> input) {
        if(input.get(0).equals(" ")){
            game.gameOutput.println("load what?");
        }else{
            try {
                game.load(input.get(0));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
