package io.muzoo.ssc.zork.player;

public class Monster {

    public String name;
    public int HP;
    private int ATK;

    public Monster(String monsterName, int ATK, int HP){
        this.name = monsterName;
        this.HP = HP;
        this.ATK = ATK;
    }

    public int getATK(){
        return this.ATK;
    }
    public int getHP(){
        return this.HP;
    }
    public String getName(){
        return this.name;
    }

    public boolean isDead(){
        return this.HP < 0;
    }

    public void attack(Player player){
        System.out.println("Monster attack back, Player decrease by: " + this.ATK + ", Player Current HP: " + player.HP);
        player.HP -= this.ATK;
    }
}
