package io.muzoo.ssc.zork.player;
import io.muzoo.ssc.zork.item.Item;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Player{

    private static final int INCREASE_HP = 5;
    private static final int MAX_HP = 200;
    public int HP;
    public int ATK;
    private List<Item> inventory;

    public Player(){
        this.inventory = new ArrayList<>();
        this.ATK = 20;
        this.HP = 100;
    }


    public Player(int ATK, int HP){
        this.inventory = new ArrayList<>();
        this.HP = HP;
        this.ATK = ATK;
    }

    public void printPlayerStat(){
        System.out.println("Player Stats: [ATK: " + ATK + " HP: " + HP +"]");
        System.out.println("Player Inventory: ---------------------------------");
        for(Item item: inventory){
            item.printItemStat();
        }
        System.out.println("---------------------------------------------------");
    }

    public void setInventory(ArrayList<Item> inventory){
        this.inventory = inventory;
    }

    public boolean isDead(){
        return this.HP < 0;
    }

    public void increaseHP(){
        this.HP += INCREASE_HP;
        if (this.HP > MAX_HP){
            this.HP = MAX_HP;
        }
    }

    public void increaseATK(){
        this.ATK++;
    }

    public void attackWith(Item item, Monster monster){
       if (inventory.contains(item)){
           item.use(this);
           monster.HP -= this.ATK;
           System.out.println("Monster HP decrease by: " + this.ATK + " Monster Current HP: " + monster.HP);
           this.ATK -= item.getATK();
       }else{
           System.out.println("No " + "\"" +item.getItemName() + "\"" + " Exist in the inventory!" );
       }
    }

    public void take(Item item){
        inventory.add(item);
    }

    public void drop(Item item){
        inventory.remove(item);
    }
    // get item from player inventory from input string
    public Item getItem(String itemName){
        for(Item item:this.inventory){
            if(item.getItemName().equalsIgnoreCase(itemName)){
                return item;
            }
        }
        System.out.println("No such item called: " + itemName + " in the inventory");
        return null;
    }


    public List<Item> getInventory(){
        return this.inventory;
    }

}
