package io.muzoo.ssc.zork.room;
import io.muzoo.ssc.zork.player.Monster;
import io.muzoo.ssc.zork.item.Item;
import io.muzoo.ssc.zork.player.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Room {

    public String roomName;
    private String description;
    private List<Item> itemList;
    private List<Monster> monsterList;
    private HashMap<String, Room> exits;

    public Room(String roomName){
        this.roomName = roomName;
    }

    public Room(){
        itemList = new ArrayList<>();
        exits = new HashMap<>();
        monsterList = new ArrayList<>();
    }

    public String getStringItems(){
        String result = "[ ";
        for(Item item: itemList){
            result += item.itemName;
            result += " ";
        }
        result += "]";
        return result;
    }

    public List<Monster> getRoomMonster(){
        return this.monsterList;
    }

    public void setRoomName(String roomName){
        this.roomName = roomName;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String exitString() {
        String returnString = "";
        Set<String> keys = exits.keySet();
        for (String key : keys) returnString += " " + key;
        return returnString;
    }

    public void removeRoomItem(Item item){
        if(itemList.contains(item)){
            itemList.remove(item);
        }
    }

    public void putItem(Item item){
        itemList.add(item);
    }

    public HashMap<String, Room> getExits(){
        return this.exits;
    }

    public void setExit(String direction, Room room){
        exits.put(direction,room);
    }

    public void putMonster(Monster monster){
        monsterList.add(monster);
    }

    public List<Item> getRoomInventory() {
        return itemList;
    }

    public Item getItem(String itemName){
        for(Item item:this.itemList){
            if(item.getItemName().equalsIgnoreCase(itemName)){
                return item;
            }
        }
        System.out.println("No such item called: " + itemName + " in this room");
        return null;
    }

    public void printRoomInfo(){
        System.out.println("This is: " + this.roomName);
        System.out.println("Room description: " + this.description);
        System.out.println("Exits: [" + exitString() + " ]");
        System.out.println("Monster count: " + monsterList.size());
        String listItem = "";
        for(Item item : itemList){
            listItem += item.itemName + " ";
        }
        System.out.println("Items in this room: [ " + listItem + " ]");
        System.out.println("***************************************************");
    }
}
