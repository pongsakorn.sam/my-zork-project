package io.muzoo.ssc.zork;

import io.muzoo.ssc.zork.game.Zork;

public class Main {

    public static void main(String[] args) {
        Zork game = new Zork();
        game.run();
    }

}
