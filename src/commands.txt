info-print out information of the player and the room that the player is currently in, this command only available while playing game.-true
take-take command is used to pick up the item in the current room, this command only available while playing game.-true
drop-drop item of choice that the player currently carries, this command only available while playing game.-true
attack with-is used to attack a monster in the current room, this command only available while playing game.-true
go-move player to the room as specified by the direction, e.g. north, east, west, south, this command only available while playing game.-true
map-print 2D map using ascii art, this command only available while playing game.-true
autopilot-run this game in autopilot mode using the list of command in the file, this command only available while playing game.-true
help-print all commands.-true
quit-end the current game and return to command prompt to let user choose the map or load from saved point again.-true
play-play new game, this command only available at when start the game.-false
load-load game state from saved point, this command only available at when start the game.-false
save-load game state from saved point, this command only available while playing game.-false
exit-exit whole game, this command only available at when start the game.-false
available exit-print out the available exit of the player current's room.-true
maps-print game's available map.-false

