Room Name: Surgery room
Room Description: You are inside the surgery room. try to find a passage out
Exit Rooms: East-Hallway, Up-Vent
Number of Monsters: 1
Number of Items: 2
Room Name: Hallway
Room Description: You are in the hallway, try not to be here, monster are everywhere
Exit Rooms: West-Surgery room, Up-Vent
Number of Monsters: 1
Number of Items: 2
Room Name: Lobby
Room Description: You are in the lobby, there is door everywhere, try entering one.
Exit Rooms: North-Hallway
Number of Monsters: 3
Number of Items: 2
Room Name: Vent
Room Description: The vent is connected to the whole building. This can bring you to anywhere you want, There is no monster in the vent.
Exit Rooms: Down-Surgery room, Up-Hallway, West-Lobby
Number of Monsters: 0
Number of Items: 0